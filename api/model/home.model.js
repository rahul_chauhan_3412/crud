const mongoose = require('../../common/services/mongoose.service').mongoose;

const Schema = mongoose.Schema;

const homeSchema = new Schema({
    email2: {type: String,},
    password2: {type: String},
    createdAt:{type: String},
    fullUrl:{type:String}
});


const home = mongoose.model('tbl_home', homeSchema);




exports.savehome = (Data) => {

    console.log(Data);
    const homeSave = new home({
        email2 : Data.email2,
        password2 : Data.password2,
        fullUrl:Data.fullUrl
        
        
    });
    return homeSave.save();

};


exports.gethome= () => {
    return new Promise((resolve, reject) => {
        home.find({})
            .exec(function (err, home) {
                if (err) {
                    reject(err);
                } else {
                    resolve(home);
                }
            })
    });
};

exports.deleteUser= (id) => {
    return new Promise((resolve, reject) => {
        home.deleteOne({_id:id})
            .exec(function (err, home) {
                if (err) {
                    reject(err);
                } else {
                    resolve(home);
                }
            })
    });
};

exports.getUserById= (id) => {
    return new Promise((resolve, reject) => {
        home.find({_id:id})
            .exec(function (err, home) {
                if (err) {
                    reject(err);
                } else {
                    resolve(home);
                }
            })
    });
}
 exports.updateUser= (data) => {
     console.log(data)
    return new Promise((resolve, reject) => {
        home.updateOne({_id:data.user_id}, { email2: data.email_update,password2:data.password_update })
            .exec(function (err, home) {
                if (err) {
                    reject(err);
                } else {
                    resolve(home);
                }
            })
    });
};


