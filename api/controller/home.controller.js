const homeModel = require('../model/home.model');
var multer  = require('multer')


exports.gethome = async (req, res) => {
    try{
           
           
        homeModel.gethome()
              .then((result) => {
                console.log(result)
                 res.render('home.ejs',{data:result});
              });

    }catch(err){
        console.log(err)
   
           return res.send({status:false, message: 'Something went wrong!'});

    }

         
   
};

var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./upload");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});


var upload = multer({
    storage: Storage
}).array("file", 3); //


exports.home = async (req, res) => {

    upload(req, res, function(err, result) {
        if (err) {
            console.log(err)
            return res.end("Something went wrong!");
        }

        if(req.files.length == 0) {
            var fullUrl = '';

        } else {
            var fullUrl = req.protocol + '://' + req.get('host')+ '/'+req.files[0].filename;

        }

        console.log(req.files)

        console.log(fullUrl)
    try{
                 req.body.fullUrl = fullUrl;
                 homeModel.savehome(req.body)
                 .then((result) => {
                     return res.send({status:true, message: "User Saved"});
                 });

    }catch(err){
        console.log(err)
   
           return res.send({status:false, message: 'Something went wrong!'});

    }

});     
   
};

exports.delete = async (req, res) => {
    console.log(req.query)
    try{
        homeModel.deleteUser(req.query.id)
        .then((result) => {
          console.log(result)
          return res.send({status:true, message: 'Deleted successfully!'});
        });
    }catch(err){
        return res.send({status:false, message: 'error!'});
    }


};

exports.getUserById = async (req, res) => {
    try{
              console.log(req.params)
           
        homeModel.getUserById(req.params.id)
              .then((result) => {
                 res.send({status:true,data:result});
              });

    }catch(err){
        console.log(err)
           return res.send({status:false, message: 'Something went wrong!'});

    }

         
   
};
 exports.userUpdate = async (req, res) => { 
   try{
        homeModel.updateUser(req.body)
        .then((result) => {
          console.log(result)
          return res.send({status:true, message: 'Update successfully!'});
        });
    }catch(err){
        return res.send({status:false, message: 'error!'});
    }


}; 



