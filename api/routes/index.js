const homecontroller = require('../controller/home.controller');

exports.routesConfig = function (app) {

  
    app.get('/home', [
        homecontroller.gethome
        ]);

    app.post('/api/home/add', [
        homecontroller.home
        ]); 
        
    app.delete('/api/home/add',[
        homecontroller.delete
        ]);  

    app.get('/api/get-user-by-id/:id',[
        homecontroller.getUserById
        ]);
        
    app.put('/api/home/user-update',[
        homecontroller.userUpdate
        ]);

   
};
