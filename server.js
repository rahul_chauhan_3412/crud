const express = require('express');
const path = require('path');

const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());
//Routes
const Routes = require('./api/routes/index');
app.set('view engine','ejs');
app.set('views', path.join(__dirname, '/views'));
app.use(express.static('upload'))


Routes.routesConfig(app);



app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});