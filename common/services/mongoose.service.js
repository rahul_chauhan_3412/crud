const mongoose = require('mongoose');
const config = require('../config/env.config');
let count = 0;

const options = {
//    autoIndex: false, // Don't build indexes
 //   poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
 //   bufferMaxEntries: 0,
    // all other approaches are now deprecated by MongoDB:
    useNewUrlParser: true,
    useUnifiedTopology: true
    
};


const connectWithRetry = () => {
    console.log('MongoDB connection with retry')
    //old databse
    //"mongodb+srv://lbdevelopment:CIgMuVf1AdWPK8c8@cluster0.9ysqj.mongodb.net/LB_MVP_LOUNCH?retryWrites=true&w=majority"
    //const url = `mongodb://${config.MONGO_USERNAME}:${config.MONGO_PASSWORD}@${config.MONGO_HOSTNAME}:${config.MONGO_PORT}/${config.MONGO_DB}?authSource=admin`;
    const url = `mongodb+srv://${config.MONGO_USERNAME}:${config.MONGO_PASSWORD}@cluster0.xjv3d.mongodb.net/${config.MONGO_DB}?retryWrites=true&w=majority
    `;
    
    mongoose.connect(url, options).then(()=>{
        console.log('MongoDB is connected')
    }).catch(err=>{
        console.log(err)
        console.log('MongoDB connection unsuccessful, retry after 5 seconds. ', ++count);
        setTimeout(connectWithRetry, 5000)
    })
};

connectWithRetry();

exports.mongoose = mongoose;
